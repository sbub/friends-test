
import React, { Component } from 'react';
import {
  AppRegistry,
  NavigatorIOS,
  StyleSheet,
  Text
} from 'react-native';

import App from './app/App';

export default class FriendsTest extends Component {

  render() {
    return (
      <NavigatorIOS
        initialRoute={{
          component: App,
          title: 'Friends'
        }}
        style={styles.container}
      />
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
});

AppRegistry.registerComponent('FriendsTest', () => FriendsTest);
